;; http://www.emacswiki.org/emacs/ELPA
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)
(defvar my-packages '(
                      auto-complete
                      smartrep
                      git-gutter
                      undo-tree
                      tabbar
                      htmlize ;; M-x htmlize-buffer
                      ag
                      wgrep
                      wgrep-ag
))
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))


(defvar rootpath (expand-file-name "~/.emacs.d"))
(defvar elisppath (concat rootpath "/elisp"))
(defvar themespath (concat rootpath "/themes"))
(defvar slidemode 0)
(setq load-path (cons elisppath load-path))
(defvar localenv-file (concat rootpath "/localenv.el"))
(if (file-exists-p localenv-file)
    (load localenv-file))

;;;; init.elを書くために使うユーティリティ
;; submodule関連
(defun submodule (dir)
  (push (format "%s/%s" elisppath dir) load-path))
(defun require-submodule (name &optional dir)
  (push (format "%s/%s" elisppath (if (null dir) name dir)) load-path)
  (require name))

(defun describe-face-at-point ()
 (interactive)
 (message "%s" (get-char-property (point) 'face)))

;; http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun get-string-from-file (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))

;;;; ファイル周りの振る舞いを変更する便利機能
;; C-x C-fやC-x C-wで指定したファイルが存在しなければディレクトリを含めて作成
(defadvice find-file
  (before find-file-mkdir (file &optional WILDCARDS) activate)
  (let ((dirname (file-name-directory file)))
    (if (and dirname
         (not (file-directory-p dirname))
         (not (string-match "/ssh:" dirname)))
    (if (not (= 0 (call-process "mkdir" nil nil nil "-p"
                    (expand-file-name dirname))))
        (error "mkdir %s failed." dirname)))))
(defadvice write-file
  (before write-file-mkdir (file &optional WILDCARDS) activate)
  (let ((dirname (file-name-directory file)))
    (if (and dirname
         (not (file-directory-p dirname))
         (not (string-match "/ssh:" dirname)))
    (if (not (= 0 (call-process "mkdir" nil nil nil "-p"
                    (expand-file-name dirname))))
        (error "mkdir %s failed." dirname)))))

;; auto-insert
(add-hook 'find-file-hooks 'auto-insert)
(setq auto-insert-directory (concat rootpath "/templates/"))
(setq auto-insert-query nil) ;; インタラクティブにy/n選択させずにすぐ挿入
(setq auto-insert-alist
      '(
        (python-mode . "templates.py")
        ))


;; 保存時、ファイルの先頭に #! があれば実行属性を付加
(defun make-file-executable ()
  "Make the file of this buffer executable, when it is a script source."
  (save-restriction
    (widen)
    (if (string= "#!" (buffer-substring-no-properties 1 (min 3 (point-max))))
        (let ((name (buffer-file-name)))
          (or (equal ?. (string-to-char (file-name-nondirectory name)))
              (let ((mode (file-modes name)))
                (set-file-modes name (logior mode (logand (/ mode 4) 73)))
                (message (concat "Wrote " name " (+x)"))))))))
(add-hook 'after-save-hook 'make-file-executable)


;; Emacsで現在開いてるバッファを再読込する - Qiita
;; http://qiita.com/ironsand/items/749b032d33d389972b4b
(defun revert-buffer-no-confirm (&optional force-reverting)
  "Interactive call to revert-buffer. Ignoring the auto-save
 file and not requesting for confirmation. When the current buffer
 is modified, the command refuses to revert it, unless you specify
 the optional argument: force-reverting to true."
  (interactive "P")
  ;;(message "force-reverting value is %s" force-reverting)
  (if (or force-reverting (not (buffer-modified-p)))
      (revert-buffer :ignore-auto :noconfirm)
    (error "The buffer has been modified")))
;; reload buffer
(global-set-key (kbd "<f5>") 'revert-buffer-no-confirm)


;;;; 操作を変更する便利関数
;; smartrep
;; http://sheephead.homelinux.org/2011/12/19/6930/
(require 'smartrep)

;; global-set-key関連
;; C-hをbackspaceに
(global-set-key (kbd "C-h") 'delete-backward-char)
;; region選択をしていない時のC-wを前の単語の削除に
(defadvice kill-region (around kill-word-or-kill-region activate)
  (if (and (interactive-p) transient-mark-mode (not mark-active))
      (backward-kill-word 1)
    ad-do-it))


;; yes-no を y-n に置き換え
(fset 'yes-or-no-p 'y-or-n-p)

;; リストを省略せずに表示
(setq eval-expression-print-length nil)
(setq eval-expression-print-level nil)

;; undo/redo
;; Undo の回数
(setq undo-limit 10000000)
(setq undo-strong-limit 13000000)
;;;; undo-tree
(global-undo-tree-mode)

;; 選択領域を入力で置き換える
(delete-selection-mode t)

;; 大文字小文字無視関連
;; 検索の時に大文字小文字を区別する
(setq-default case-fold-search nil)
;; 置換の時に大文字小文字を区別する
(setq-default case-replace nil)
;; 補完時に大文字小文字を区別しない
(setq read-file-name-completion-ignore-case t)

;;インデントでタブを使わない
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

(setq-default c-basic-offset 4)

;; 行の先頭でC-kを一回押すだけで行全体を消去する(設定しないと何もない行が無駄に残る)
(setq kill-whole-line t)


;; 分割したバッファーをF2で入れ替える
(defun swap-screen()
  "Swap two screen,leaving cursor at current window."
  (interactive)
  (let ((thiswin (selected-window))
        (nextbuf (window-buffer (next-window))))
    (set-window-buffer (next-window) (window-buffer))
    (set-window-buffer thiswin nextbuf)))
(defun swap-screen-with-cursor()
  "Swap two screen,with cursor in same buffer."
  (interactive)
  (let ((thiswin (selected-window))
        (thisbuf (window-buffer)))
    (other-window 1)
    (set-window-buffer thiswin (window-buffer))
    (set-window-buffer (selected-window) thisbuf)))
(global-set-key [f2] 'swap-screen)
(global-set-key [S-f2] 'swap-screen-with-cursor) ;; Shift-F2

;; clipboardに保存
(cond (window-system
       (setq x-select-enable-clipboard t)))

;; dired でサイズ，拡張子で並び換えの改良版
(add-hook 'dired-load-hook
          (lambda () (require 'sorter)))

;; CUAで矩形切り取りした後、Emacs以外でコピーしたテキストを張り付けると
;; 挙動がおかしいので、標準の矩形選択（C-x r k / C-x r y）を使うことにした
;; 矩形選択を直感的に
;; http://dev.ariel-networks.com/articles/emacs/part5
(cua-mode t)
;; C-cやC-vの乗っ取りを阻止
(setq cua-enable-cua-keys nil)

;; auto-complete
;; 基本設定
(require 'auto-complete-config)
;; パス入力用の情報源を追加
(defun ac-common-setup ()
  (add-to-list 'ac-sources 'ac-source-filename))
;; ac-source-filename は ac-sources の先頭でないと "~/" が上手く補完できないため，
;; ac-sources の後ろに mode-hook 用情報源を付け足すよう変更
(defun ac-emacs-lisp-mode-setup ()
  (setq ac-sources (append ac-sources '(ac-source-features ac-source-functions ac-source-yasnippet ac-source-variables ac-source-symbols))))
(defun ac-cc-mode-setup ()
  (setq ac-sources (append ac-sources '(ac-source-yasnippet ac-source-gtags))))
(defun ac-css-mode-setup ()
  (setq ac-sources (append ac-sources '(ac-source-css-property))))
;; 設定した ac-source を適用する
(ac-config-default)
;; カスタマイズ
(setq ac-auto-start 2)  ;; n文字以上の単語の時に補完を開始
(setq ac-delay 0.05)  ;; n秒後に補完開始
(setq ac-use-fuzzy t)  ;; 曖昧マッチ有効
(setq ac-use-comphist t)  ;; 補完推測機能有効
(setq ac-auto-show-menu 0.05)  ;; n秒後に補完メニューを表示
(setq ac-quick-help-delay 0.5)  ;; n秒後にクイックヘルプを表示
(setq ac-ignore-case nil)  ;; 大文字・小文字を区別する

;; auto-complete の候補に日本語を含む単語が含まれないようにする
;; http://d.hatena.ne.jp/IMAKADO/20090813/1250130343
(defadvice ac-word-candidates (after remove-word-contain-japanese activate)
  (let ((contain-japanese (lambda (s) (string-match (rx (category japanese)) s))))
    (setq ad-return-value
          (remove-if contain-japanese ad-return-value))))

;;;; ag + wgrep
;; http://codeout.hatenablog.com/entry/2014/07/08/185826
; ag
(setq default-process-coding-system 'utf-8-unix)  ; ag 検索結果のエンコード指定
;; agの検索結果でn/pを押せば、対応する箇所が別Bufferに表示されるようにする
(add-hook 'ag-mode-hook (lambda () (next-error-follow-minor-mode)))
(setq ag-highlight-search t)  ; 検索キーワードをハイライト
(setq ag-reuse-buffers t)     ; 検索用バッファを使い回す (検索ごとに新バッファを作らない)
; wgrep
(add-hook 'ag-mode-hook '(lambda ()
                           (setq wgrep-enable-key "r")      ; "r" キーで編集モードに
                           (wgrep-ag-setup)))

;;;; git-gutter
;; http://qiita.com/syohex/items/a669b35fbbfcdda0cbf2
(global-git-gutter-mode +1)
;; use git-gutter.el and linum-mode
(git-gutter:linum-setup)
;; Jump to next/previous hunk
(smartrep-define-key
    global-map  "C-x" '(("p" . 'git-gutter:previous-diff)
                        ("n" . 'git-gutter:next-diff)))
;; Stage current hunk
(global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)
;; Revert current hunk
(global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)


;;;; デザイン関連
;; 複数ウィンドウを開かないようにする
(if window-system
    (setq ns-pop-up-frames nil))
;;起動時のフレームサイズを設定する
(setq initial-frame-alist
      (append (list
               '(width . 99)
               '(height . 45)
               )
              initial-frame-alist))
(setq default-frame-alist initial-frame-alist)

;; fontsizeが変わったらフレームサイズは記憶したものを使わない
(defvar fontsizefile (concat rootpath "/fontsize.txt"))
(defvar pfontsizefile (concat rootpath "/.fontsize.txt"))

(defvar fontsize
  (string-to-number
   (get-string-from-file
    fontsizefile)))
(defvar pfontsize
  (string-to-number
   (get-string-from-file
    pfontsizefile)))
(defun my-font-size-save ()
  (setq buf (get-file-buffer (expand-file-name pfontsizefile)))
  (if (not buf)
      (setq buf (find-file-noselect pfontsizefile)))
  (set-buffer buf)
  (erase-buffer)
  (insert (number-to-string fontsize))
  (save-buffer)
  )

;; フレームサイズと位置を記憶
(defvar framesize-file (concat rootpath "/.framesize.el"))
(defvar my-session-set-file-name-exclude-regexp
  "[/\\]\\.framesize\\.el\\|[/\\]\\.fontsize\\.txt")

(defun my-window-size-save ()
  (let* ((rlist (frame-parameters (selected-frame)))
         (ilist initial-frame-alist)
         (nCHeight (frame-height))
         (nCWidth (frame-width))
         (tMargin (if (integerp (cdr (assoc 'top rlist)))
                      (cdr (assoc 'top rlist)) 0))
         (lMargin (if (integerp (cdr (assoc 'left rlist)))
                      (cdr (assoc 'left rlist)) 0))
         buf
         (file framesize-file))
    (setq buf (get-file-buffer (expand-file-name file)))
    (if (not buf)
        (setq buf (find-file-noselect file)))
    (set-buffer buf)
    (erase-buffer)
    (insert (concat
             ;; 初期値をいじるよりも modify-frame-parameters
             ;; で変えるだけの方がいい?
             "(delete 'width initial-frame-alist)\n"
             "(delete 'height initial-frame-alist)\n"
             "(delete 'top initial-frame-alist)\n"
             "(delete 'left initial-frame-alist)\n"
             "(setq initial-frame-alist (append (list\n"
             "'(width . " (int-to-string nCWidth) ")\n"
             "'(height . " (int-to-string nCHeight) ")\n"
             "'(top . " (int-to-string tMargin) ")\n"
             "'(left . " (int-to-string lMargin) "))\n"
             "initial-frame-alist))\n"
             ;;"(setq default-frame-alist initial-frame-alist)"
             ))
    (save-buffer)
    ))
(defun my-window-size-load ()
  (let* ((file framesize-file))
    (if (file-exists-p file)
        (load file))))

;; fontsize.txtで定義したフォントサイズが
;; 前回のフォントサイズと異なる場合、
;; my-window-size-loadによって、フレームが画面からはみ出す問題がある。
;; そこで前回のフォントサイズより3以上異なるサイズになったり、
;; フォントサイズが20以上の場合は、width=60, height=40とする
(if (or (< (abs (- fontsize pfontsize)) 3)
       (< fontsize 20))
    (my-window-size-load)
 (progn
   (setq initial-frame-alist
         (append (list
                  '(width . 60)
                  '(height . 40)
                  )
                 initial-frame-alist))
   (setq default-frame-alist initial-frame-alist)
  )
)

;; Call the function above at C-x C-c.
(defadvice save-buffers-kill-emacs
  (before save-frame-font-size activate)
  (if window-system
      (progn
        (my-window-size-save)
        (my-font-size-save))))

;; .framesize.el, .fontsize.txtを.sessionに保存しない
(when (require 'session nil t)
  (custom-set-variables
   '(session-set-file-name-exclude-regexp
     (concat session-set-file-name-exclude-regexp "\\|" my-session-set-file-name-exclude-regexp))))

;; スタートアップ非表示
(setq inhibit-startup-screen t)
;; メニューバーを非表示
(menu-bar-mode -1)
;; ツールバー非表示
(when window-system
  (tool-bar-mode 0))
;; 右端のスクロールバーを非表示
(when window-system
  (toggle-scroll-bar nil))
;; frameの左側の余白を削除
(fringe-mode (cons 0 nil))
;; *scratch*で最初に書かれるmessage
(setq initial-scratch-message ";; -*- coding:utf-8-unix -*-\n;; *scratch* buffer\n\n")
;; no beep
(setq ring-bell-function 'ignore)
;;カーソルの点滅を停止
(blink-cursor-mode 0)
;; 現在行をハイライト表示
(global-hl-line-mode 1)
;;(set-face-background 'hl-line "#303030")
;; 対応する括弧を強調表示
(show-paren-mode t)
;; ウィンドウ内に収まらない時だけ括弧内も強調表示
;; (setq show-paren-style 'mixed) ;; 今のところ不要
;; 選択領域 を強調
;; (transient-mark-mode t) ;; no need over emacs24?
;; 行・カラム関連
(line-number-mode 0)
(column-number-mode 1)
;; 行番号を表示 (linum-mode)
(require 'linum)
(global-linum-mode t)      ; デフォルトで linum-mode を有効にする
(setq linum-format "%6d ") ; n桁分の領域を確保して行番号のあとにスペースを入れる
;; 背景透過
(set-frame-parameter nil 'alpha 85)

;; ファイル名が重複していたらバッファ名にディレクトリ名を追加
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; タイトルをカスタマイズ
(setq frame-title-format '("" "%b @ Emacs " emacs-version))

;; theme
(if window-system
  (load (concat themespath "/aria-palette.el"))
  (load (concat themespath "/aria-palette256.el")))
(add-to-list 'custom-theme-load-path themespath)
(cond
 ((eq slidemode 1)
  (load-theme 'whiteboard t))
 ((eq slidemode 0)
  (load-theme 'aria t)))

;; 全角スペース タブ trailing-spacesを目立たせる
(require 'whitespace)
;; space-markとtab-mark、それからspacesとtrailingを対象とする
(setq whitespace-style '(space-mark tab-mark face spaces trailing))
(setq whitespace-display-mappings
      '(
        ;; (space-mark   ?\     [?\u00B7]     [?.]) ; space - centered dot
        (space-mark   ?\xA0  [?\u00A4]     [?_]) ; hard space - currency
        (space-mark   ?\x8A0 [?\x8A4]      [?_]) ; hard space - currency
        (space-mark   ?\x920 [?\x924]      [?_]) ; hard space - currency
        (space-mark   ?\xE20 [?\xE24]      [?_]) ; hard space - currency
        (space-mark   ?\xF20 [?\xF24]      [?_]) ; hard space - currency
        (space-mark ?\u3000 [?\u25a1] [?_ ?_]) ; full-width-space - square
        ;; NEWLINE is displayed using the face `whitespace-newline'
        ;; (newline-mark ?\n    [?$ ?\n])	; eol - dollar sign
        ;; (newline-mark ?\n    [?\u21B5 ?\n] [?$ ?\n])	; eol - downwards arrow
        ;; (newline-mark ?\n    [?\u00B6 ?\n] [?$ ?\n])	; eol - pilcrow
        ;; (newline-mark ?\n    [?\x8AF ?\n]  [?$ ?\n])	; eol - overscore
        ;; (newline-mark ?\n    [?\x8AC ?\n]  [?$ ?\n])	; eol - negation
        ;; (newline-mark ?\n    [?\x8B0 ?\n]  [?$ ?\n])	; eol - grade
        ;;
        ;; WARNING: the mapping below has a problem.
        ;; When a TAB occupies exactly one column, it will display the
        ;; character ?\xBB at that column followed by a TAB which goes to
        ;; the next TAB column.
        ;; If this is a problem for you, please, comment the line below.
        (tab-mark     ?\t    [?\u00BB ?\t] [?\\ ?\t]) ; tab - left quote mark
        ))
;; whitespace-spaceの定義を全角スペースにし、色をつけて目立たせる
(setq whitespace-space-regexp "\\(\u3000+\\)")
;;(set-face-foreground 'whitespace-space "cyan")
;;(set-face-background 'whitespace-space 'nil)
;; whitespace-trailingを色つきアンダーラインで目立たせる
(set-face-underline  'whitespace-trailing t)
;;(set-face-foreground 'whitespace-trailing "cyan")
;;(set-face-background 'whitespace-trailing 'nil)
(global-whitespace-mode 1)

;; 80 桁目に縦線
(require-submodule 'fill-column-indicator)
(setq-default fci-rule-column 80)
(setq fci-rule-width 5)
;; (setq fci-rule-color "pink")
(setq fci-rule-color (aria-color "Renaissance"))
(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode 1)

;; tabbar.el
(tabbar-mode 1)
;; Ctrl-Tabで次、Ctrl-Shift-Tabで前のタブに切り替え
(global-set-key (kbd "<C-tab>") 'tabbar-forward-tab)
(global-set-key (kbd "<C-S-tab>") 'tabbar-backward-tab)
;; タブ上でマウスホイール操作無効
(tabbar-mwheel-mode -1)
;; グループ化しない
(setq tabbar-buffer-groups-function nil)
;; 左に表示されるボタンを無効化
(dolist (btn '(tabbar-buffer-home-button
               tabbar-scroll-left-button
               tabbar-scroll-right-button))
  (set btn (cons (cons "" nil)
                 (cons "" nil))))
;; タブ同士の間隔
(setq tabbar-separator '(0.8))
;; ;; 外観変更
;; (set-face-attribute
;;  'tabbar-default nil
;;  :family (face-attribute 'default :family)
;;  :background (face-attribute 'mode-line-inactive :background)
;;  :height 0.9)
;; (set-face-attribute
;;  'tabbar-unselected nil
;;  :background (face-attribute 'mode-line-inactive :background)
;;  :foreground (face-attribute 'mode-line-inactive :foreground)
;;  :box nil)
;; (set-face-attribute
;;  'tabbar-selected nil
;;  :background (face-attribute 'mode-line :background)
;;  :foreground (face-attribute 'mode-line :foreground)
;;  :box nil)
;; タブに表示させるバッファの設定
(defvar my-tabbar-displayed-buffers
  '("*scratch*")
  "*Regexps matches buffer names always included tabs.")
(defun my-tabbar-buffer-list ()
  "Return the list of buffers to show in tabs.
Exclude buffers whose name starts with a space or an asterisk.
The current buffer and buffers matches `my-tabbar-displayed-buffers'
are always included."
  (let* ((hides (list ?\  ?\*))
         (re (regexp-opt my-tabbar-displayed-buffers))
         (cur-buf (current-buffer))
         (tabs (delq nil
                     (mapcar (lambda (buf)
                               (let ((name (buffer-name buf)))
                                 (when (or (string-match re name)
                                           (not (memq (aref name 0) hides)))
                                   buf)))
                             (buffer-list)))))
    ;; Always include the current buffer.
    (if (memq cur-buf tabs)
        tabs
      (cons cur-buf tabs))))
(setq tabbar-buffer-list-function 'my-tabbar-buffer-list)


;;;; バッファ,履歴周り
;; バッファ一覧をまともなものに
(global-set-key (kbd "C-x C-b") 'bs-show)
(setq bs-default-configuration "files-and-scratch");;scratchも表示

;; iswitchbを使いバッファの切り替え時にインクリメンタル補完
;; http://ubulog.blogspot.com/2007/09/emacs_19.html
(iswitchb-mode 1) ;;iswitchbモードON
;; C-f, C-b, C-n, C-p で候補を切り替えることができるように。
(add-hook 'iswitchb-define-mode-map-hook
          (lambda ()
            (define-key iswitchb-mode-map "\C-n" 'iswitchb-next-match)
            (define-key iswitchb-mode-map "\C-p" 'iswitchb-prev-match)
            (define-key iswitchb-mode-map "\C-f" 'iswitchb-next-match)
            (define-key iswitchb-mode-map "\C-b" 'iswitchb-prev-match)))


;; M-x の補完を iswitchb のようにしたい
;; M-x などをヒストリで補完したい
;; ミニバッファ補完 M-x (mcomplete.el)
(require-submodule 'mcomplete)
(load "mcomplete-history")
(turn-on-mcomplete-mode)

;; session
;; http://openlab.dino.co.jp/2008/09/26/230919351.html
;; ミニバッファ履歴リストの最大長：tなら無限
(setq history-length t)
;; session.el
;; kill-ringやミニバッファで過去に開いたファイルなどの履歴を保存する
(when (require 'session nil t)
  (setq session-save-file-coding-system 'utf-8-unix) ;; 追加
  (setq session-initialize '(de-saveplace session keys menus places)
        session-globals-include '((kill-ring 50)
                                  (session-file-alist 500 t)
                                  (file-name-history 10000)))
  (add-hook 'after-init-hook 'session-initialize)
  ;; 前回閉じたときの位置にカーソルを復帰
  (setq session-undo-check -1))

;; find-fileでTABで一覧を表示した時に表示しないファイルを設定
;; http://epian-wiki.appspot.com/wiki/Memo/20100828085804/Emacs:find-file%E6%99%82%E3%81%AE%E4%B8%80%E8%A6%A7%E3%81%AB%E3%83%90%E3%83%83%E3%82%AF%E3%82%A2%E3%83%83%E3%83%97%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%82%92%E8%A1%A8%E7%A4%BA%E3%81%97%E3%81%AA%E3%81%84
;; http://stackoverflow.com/questions/1731634/dont-show-uninteresting-files-in-emacs-completion-window
(setq completion-ignored-extensions
      (append completion-ignored-extensions
              ;; 表示したくないファイルの末尾を追加 e.g. ".pyc"
              '("#")))
(defadvice completion-file-name-table (after ignoring-backups-f-n-completion activate)
  "filter out results when the have completion-ignored-extensions"
  (let ((res ad-return-value))
    (if (and (listp res)
          (stringp (car res))
          (cdr res)) ; length > 1, don't ignore sole match
        (setq ad-return-value
          (completion-pcm--filename-try-filter res)))))
(eval-after-load "dired"
  '(require 'dired-x))
(add-hook 'dired-mode-hook
          (lambda ()
            (dired-omit-mode 1)))


;;;; font
(cond
 ((eq window-system 'x)
  (progn
    (set-frame-font (format "Ricty-%d" fontsize))
    (set-fontset-font
     (frame-parameter nil 'font)
     'japanese-jisx0208
     (cons my-font-name "unicode-bmp")
     )))
 ((eq window-system 'ns)
  (progn
    (create-fontset-from-ascii-font
     (format "Ricty-%d:weight=normal:slant=normal" fontsize) nil "ricty")
    (set-fontset-font "fontset-ricty" 'unicode (font-spec :family "Ricty" :size fontsize) nil 'append)
    (add-to-list 'default-frame-alist '(font . "fontset-ricty"))
    ))
 ((eq window-system 'w32)
  (progn
    (add-to-list 'default-frame-alist '(font . (format "ricty-%d" fontsize)))
    ))
 )

;;;; Mac
(cond ((eq window-system 'ns)
       (progn
         (setq ns-command-modifier 'super)
         (setq ns-alternate-modifier 'meta)
         (setq mac-pass-control-to-system nil)
         (setq mac-pass-command-to-system nil)
         (setq mac-pass-option-to-system nil)
         (setq mac-pass-shift-to-system nil)
      )))

;; EmacsのWindowを一番上に表示
(if (eq window-system 'ns)
    (x-focus-frame nil))

;;;; windows
(cond ((eq window-system 'w32)
       (progn
         ;; cygwinをexec-pathに追加
         (setq exec-path (cons "/cygwin/bin"  exec-path))
         ;; ~を初期ディレクトリにする
         (cd "~")
         )))

;;
(defun liteweight()
  "to read large file."
  (interactive)
  (progn
    (setq ac-auto-start nil)
    (setq show-paren-mode nil)
    (line-number-mode 1)
    (global-linum-mode -1)
    (fringe-mode (cons nil nil))
    (global-whitespace-mode -1)
    (disable-theme 'aria)
    (turn-off-fci-mode)
    (toggle-truncate-lines 1)
))

;; for Mac OS X Marvericks
(if (and (eq window-system 'ns) ;; Mac OS X
         (equal (getenv "PATH") "/usr/bin:/bin:/usr/sbin:/sbin")) ;; launch from Dock
    (progn
      (setq default-directory "~/")
      (setq command-line-default-directory "~/")))

;;;; mode hock
;; python autopep8
;; https://github.com/paetzke/py-autopep8.el
;; python-path is defined in localenv.el
(defvar do-autopep8 nil)
(if (and do-autopep8 (file-exists-p localenv-file))
    (progn
      (setq exec-path (append (list python-path) exec-path))
      (require 'py-autopep8)
      (add-hook 'before-save-hook 'py-autopep8-before-save)
      ))
