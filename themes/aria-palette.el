"This is a table of all the colors used by the ARIA color theme."
(defvar aria-colors (make-hash-table :test 'equal))

;; named colors
;; get color http://pluscolorn.sub.jp/rgbseachtest3.php
;; keyword イタリア
(puthash "Magenta2"                                  "#c03874" aria-colors)
;; (puthash "Raw-Umber"                                 "#926f61" aria-colors)
(puthash "Raw-Umber"                                 "#9f796a" aria-colors) ;; 明度上げた
(puthash "Capri-Blue"                                "#00809f" aria-colors)
(puthash "Celeste"                                   "#8bafdb" aria-colors)
(puthash "Celeste-Blue"                              "#8bafd7" aria-colors)
(puthash "Chianti"                                   "#62001a" aria-colors)
(puthash "Dante"                                     "#510023" aria-colors)
(puthash "Deep-Venice"                               "#005663" aria-colors)
(puthash "Italian-Blue"                              "#33ffff" aria-colors)
(puthash "Italian-Rose"                              "#f4506d" aria-colors)
(puthash "Sorrent-Gold"                              "#ff9e00" aria-colors)
(puthash "Tiber-Green"                               "#aaf0aa" aria-colors)
(puthash "Tuscany-Red"                               "#bd0030" aria-colors)
(puthash "Verona"                                    "#8af0cf" aria-colors)

;; keyword Vene
(puthash "Venezia"                                   "#b2e6d3" aria-colors)
(puthash "Venetian-Red"                              "#783530" aria-colors)
(puthash "Venetian-Scarlet"                          "#df113d" aria-colors)

;; keyword ルネッサンス
(puthash "Emerald-Green"                             "#01a650" aria-colors)
(puthash "Giotto-Blue"                               "#01a6d6" aria-colors)
(puthash "Malachite-Green"                           "#809c2b" aria-colors)
(puthash "Orange-Lake"                               "#f18b01" aria-colors)
(puthash "Renaissance"                               "#9f2c60" aria-colors)
(puthash "Reseda-Yellow"                             "#ffda00" aria-colors)
(puthash "Ultramarine"                               "#015aaa" aria-colors)
(puthash "Renaissance-Blue"                          "#3c1482" aria-colors)

;; keyword ワイン
(puthash "Old-Wine"                                  "#672d32" aria-colors)
(puthash "Vintage-Wine"                              "#551628" aria-colors)
(puthash "Wine"                                      "#6b0032" aria-colors)
(puthash "Wine-Color"                                "#9f356f" aria-colors)
(puthash "Vert-Bouteille"                            "#1a4d2e" aria-colors)
(puthash "Wine-Red"                                  "#80273f" aria-colors)

;; keyword 麦
(puthash "Straw"                                     "#ffebb5" aria-colors)

;; keyword さんご
(puthash "Sangoiro"                                  "#f5b1aa" aria-colors)

;; Italian-Roseとの組み合わせ
(puthash "Cyan-Blue"                                 "#5dbbb8" aria-colors)

;; Italian-Blue, Giotto-Blueとの組み合わせ
(puthash "Hyacinth-Blue"                             "#6776fe" aria-colors)

;; get color from http://www.art-paints.com/Paints/Watercolor/Venezia/Cerulean/Cerulean.html
(puthash "Cerulean"                                  "#4789C3" aria-colors)

;; spoited colors
;; get color Raffaello 大公の聖母 http://art.pro.tok2.com/R/Raphael/Raph008.htm
;; (puthash "Raffaello-Red"                             "#c52823" aria-colors)
(puthash "Raffaello-Red"                             "#cb4020" aria-colors)
;; (puthash "Raffaello-Red"                             "#ce3f03" aria-colors)
(puthash "Raffaello-Blue"                            "#76afa0" aria-colors)

;; get color ラファエロ展のタイトルの金色の字
(puthash "Raffaello-Gold"                            "#e49c00" aria-colors)

;; get color from ARIA PREMIUM POSTER BOOK3 AKARI only image
(puthash "Akari-Hair-highlight"                      "#e1bdd3" aria-colors)
(puthash "Akari-Hair"                                "#dd9dbb" aria-colors)
(puthash "Akari-Hair-deep"                           "#df7ec0" aria-colors)

(puthash "Akari-eye-highlight"                       "#b5c984" aria-colors)
(puthash "Akari-eye"                                 "#61971b" aria-colors)
(puthash "Akari-eye-deep"                            "#4b731b" aria-colors)

(puthash "Aria-company-yellow"                       "#e9c04a" aria-colors)
(puthash "Aria-company-blue-highlight"               "#aeafc3" aria-colors)
(puthash "Aria-company-blue"                         "#45569c" aria-colors)

(puthash "italia-sky-blue"                           "#004db2" aria-colors)
(puthash "italia-arch-white"                         "#c7d6dd" aria-colors)

;; get color from ARIA PREMIUM POSTER BOOK3 ALICE only image
(puthash "Orange-planet-yellow"                      "#ffee84" aria-colors)
(puthash "italia-penki-blue-light"                   "#5babe6" aria-colors)
(puthash "italia-penki-blue"                         "#2691d9" aria-colors)
(puthash "unga-green"                                "#bedfce" aria-colors)
;; (puthash "unga-green"                               "#1ae265" aria-colors)

;; get color from ARiA PREMIUM POSTER BOOK3 AIKA only image
(puthash "Aika-Hair-deep"                            "#5d7ac5" aria-colors)
(puthash "Aika-Hair"                                 "#7eaedc" aria-colors)
;; (puthash "himeya-orange"                             "#F0A080" aria-colors)
(puthash "himeya-orange"                             "#F8B981" aria-colors)

;; get color from http://vi.sualize.us/two_canals_venice_morbcn_italy_photo_venezia_colors_picture_mVNv.html
(puthash "canal-green"                               "#45b7ad" aria-colors)

(puthash "italia-red"                                "#BF4046" aria-colors)
(puthash "italia-yellow"                             "#c3b14c" aria-colors)

;; original is https://github.com/hbin/molokai-theme/blob/master/molokai-theme.el
(puthash "white"                                     "#ffffff" aria-colors)
;; (puthash "fg"                                        "#f8f8f0" aria-colors)
(puthash "fg"                                        "#faf4f4" aria-colors) ;; Raffaello2013 poster
(puthash "grey-2"                                    "#bcbcbc" aria-colors)
(puthash "grey-1"                                    "#8f8f8f" aria-colors)
(puthash "grey"                                      "#808080" aria-colors)
(puthash "grey+2"                                    "#403d3d" aria-colors)
(puthash "grey+3"                                    "#4c4745" aria-colors)
(puthash "grey+5"                                    "#232526" aria-colors)
;; (puthash "bg"                                        "#1b1d1e" aria-colors)
(puthash "bg"                                        "#070103" aria-colors) ;; Raffaello2013 poster
(puthash "grey+10"                                   "#080808" aria-colors)
(puthash "dark"                                      "#000000" aria-colors)
(puthash "base01"                                    "#465457" aria-colors)
(puthash "base02"                                    "#455354" aria-colors)
(puthash "base03"                                    "#293739" aria-colors)

(puthash "Soft-lavender"                             "#bfa6dc" aria-colors)


(defun aria-color (name) (gethash name aria-colors))
