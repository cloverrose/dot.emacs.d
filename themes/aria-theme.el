;;; aria-theme.el --- ARIA theme for Emacs 24

;; Author: cloverrose
;; Version: 0.1

;;; Commentary:
;;
;; This is ARIA theme for Emacs 24.
;; Equiped with my favorites.
;; Italia, Venezia, Renaissance, Raffaello

;;; Requirements:
;;
;; Emacs 24

;;; Code:

(deftheme aria "The ARIA color theme for Emacs 24")
(custom-theme-set-faces
 'aria

 ;; base
 `(default ((t (:background ,(aria-color "bg") :foreground ,(aria-color "fg")))))
 `(cursor ((t (:background ,(aria-color "fg") :foreground ,(aria-color "bg")))))
 `(fringe ((t (:background ,(aria-color "bg") :foreground ,(aria-color "base02")))))
 `(highlight ((t (:background ,(aria-color "grey+2")))))
 `(region ((t (:background  ,(aria-color "grey+2"))) (t :inverse-video t)))
 `(warning ((t (:background ,(aria-color "Sangoiro") :foreground ,(aria-color "Venetian-Scarlet") :weight bold))))

 ;; `(link ((t (:foreground ,(aria-color "Giotto-Blue") :underline (:color ,(aria-color "Giotto-Blue") :style line)))))
 `(link ((t (:foreground ,(aria-color "Italian-Blue") :underline (:color ,(aria-color "Italian-Blue") :style line)))))
 `(link-visited ((t (:foreground ,(aria-color "Hyacinth-Blue") :underline (:color ,(aria-color "Hyacinth-Blue") :style line)))))

 ;; font lock
 `(font-lock-builtin-face ((t (:foreground ,(aria-color "italia-penki-blue-light")))))
 `(font-lock-constant-face ((t (:foreground ,(aria-color "italia-penki-blue-light")))))
 `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
 `(font-lock-comment-face ((t (:foreground ,(aria-color "Raw-Umber") :slant italic))))
 ;; `(font-lock-comment-face ((t (:foreground ,(aria-color "Raffaello-Red") :slant italic))))  ;; for emphasis comment
 `(font-lock-comment-delimiter-face ((t (:inherit (font-lock-comment-face)))))
 `(font-lock-doc-string-face ((t (:foreground ,(aria-color "Orange-Lake")))))
 `(font-lock-doc-face ((t (:inherit font-lock-doc-string-face))))
 `(font-lock-variable-name-face ((t (:foreground ,(aria-color "Straw")))))
 ;; `(font-lock-variable-name-face ((t (:foreground ,(aria-color "Orange-planet-yellow")))))  ;; for emphasis variable
 `(font-lock-function-name-face ((t (:foreground ,(aria-color "Tiber-Green")))))
 ;; `(font-lock-type-face ((t (:foreground ,(aria-color "Cerulean") :weight bold))))
 `(font-lock-type-face ((t (:foreground ,(aria-color "Aika-Hair") :weight bold))))
 `(font-lock-keyword-face ((t (:foreground ,(aria-color "Akari-Hair") :weight bold))))
 `(font-lock-string-face ((t (:foreground ,(aria-color "himeya-orange")))))
 `(font-lock-negation-char-face ((t (:inherit font-lock-string-face))))
 `(font-lock-regexp-grouping-backslash ((t (:inherit (bold)))))
 `(font-lock-regexp-grouping-construct ((t (:inherit (bold)))))
 `(font-lock-warning-face ((t (:background ,(aria-color "Sangoiro") :foreground ,(aria-color "Venetian-Scarlet") :weight bold))))

 ;; mode line
 `(mode-line ((t (:foreground ,(aria-color "fg") :background ,(aria-color "Deep-Venice") :box nil))))
 `(mode-line-buffer-id ((t (:weight bold))))
 `(mode-line-inactive ((t (:foreground ,(aria-color "grey-2") :background ,(aria-color "grey+5") :box nil))))

 ;; mini buffer
 `(minibuffer-prompt  ((t (:foreground ,(aria-color "Akari-Hair-deep")))))

 ;; search
 `(isearch ((t (:foreground ,(aria-color "dark") :background ,(aria-color "Reseda-Yellow") :weight bold))))
 `(isearch-fail ((t (:foreground ,(aria-color "Venetian-Scarlet") :background ,(aria-color "Sangoiro")))))
 `(lazy-highlight ((t (:foreground ,(aria-color "dark") :background ,(aria-color "Venezia") :weight bold))))

 `(ediff-current-diff-A ((t (:foreground ,(aria-color "fg") :background ,(aria-color "Italian-Rose") :weight bold))))
 `(ediff-current-diff-B ((t (:foreground ,(aria-color "fg") :background ,(aria-color "Cyan-Blue") :weight bold))))

 ;; hl-line-mode
 `(hl-line ((t (:background ,(aria-color "grey+5")))))

 ;; bs-show (C-x bで出てくる)
 `(iswitchb-current-match ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(iswitchb-single-match ((t (:foreground ,(aria-color "Italian-Rose")))))

 ;; dired-directory
 `(dired-directory ((t (:foreground ,(aria-color "Aika-Hair")))))
 `(dired-perm-write ((t (:foreground ,(aria-color "Tiber-Green")))))
 `(dired-symlink ((t (:foreground ,(aria-color "Tiber-Green")))))

 ;; java mode
 `(c-annotation-face ((t (:foreground ,(aria-color "Soft-lavender")))))

 ;; linum-mode
 `(linum ((t (:foreground ,(aria-color "grey-2") :background ,(aria-color "grey+5")))))

 ;; mcomplete
 `(mcomplete-substr-method-alternative-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(mcomplete-substr-method-fixed-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(mcomplete-history-method-alternative-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(mcomplete-history-method-fixed-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(mcomplete-prefix-method-alternative-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))
 `(mcomplete-prefix-method-fixed-part-face ((t (:foreground ,(aria-color "Italian-Rose")))))

 ;; tabbar
 `(tabbar-default  ((t (:background ,(aria-color "grey+2") :foreground ,(aria-color "fg")  :height 0.9))))
 `(tabbar-selected  ((t (:background ,(aria-color "italia-arch-white") :foreground ,(aria-color "bg") :box nil))))
 `(tabbar-unselected  ((t (:background ,(aria-color "grey+2") :foreground ,(aria-color "grey-2") :box nil))))

 ;; whitespace
 `(whitespace-space ((t (:background nil :foreground ,(aria-color "Italian-Blue")))))
 `(whitespace-trailing ((t (:background nil :foreground ,(aria-color "Italian-Blue")))))

 ;; moccur (moccur-edit)
 `(moccur-face ((t (:foreground ,(aria-color "fg") :background ,(aria-color "Cyan-Blue") :weight bold)))) ;; same as lazy-highlight
 `(moccur-edit-face ((t (:foreground ,(aria-color "fg") :background ,(aria-color "Italian-Rose") :weight bold))))
 `(moccur-edit-done-face ((t (:foreground ,(aria-color "grey")))))

 )

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'aria)
