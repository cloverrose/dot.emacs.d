"This is a table of all the colors used by the ARIA color theme."
(defvar aria-colors (make-hash-table :test 'equal))

(puthash "fg"                                        "#e5e5e5" aria-colors)
(puthash "italia-arch-white"                         "#c6c6c6" aria-colors)

(puthash "bg"                                        "#000000" aria-colors)
(puthash "dark"                                      "#000000" aria-colors)
(puthash "base02"                                    "#444444" aria-colors)
(puthash "grey-2"                                    "#bcbcbc" aria-colors)
(puthash "grey+2"                                    "#444444" aria-colors)
(puthash "grey+5"                                    "#262626" aria-colors)

(puthash "Renaissance"                               "#af0087" aria-colors)
(puthash "Sangoiro"                                  "#ffafaf" aria-colors)
(puthash "Italian-Rose"                              "#ff5f5f" aria-colors)
(puthash "Venetian-Scarlet"                          "#d7005f" aria-colors)
(puthash "Akari-Hair"                                "#d7afd7" aria-colors)
(puthash "Akari-Hair-deep"                           "#d75fd7" aria-colors)

(puthash "Italian-Blue"                              "#5fffff" aria-colors)
(puthash "Hyacinth-Blue"                             "#5f87ff" aria-colors)
(puthash "italia-penki-blue-light"                   "#5fafff" aria-colors)
(puthash "Aika-Hair"                                 "#87afd7" aria-colors)

(puthash "Raw-Umber"                                 "#af875f" aria-colors)
(puthash "himeya-orange"                             "#ffaf87" aria-colors)
(puthash "Orange-Lake"                               "#ff8700" aria-colors)
(puthash "Straw"                                     "#ffd7af" aria-colors)
(puthash "Reseda-Yellow"                             "#ffd700" aria-colors)

;;(puthash "Tiber-Green"                               "#afffaf" aria-colors)
(puthash "Tiber-Green"                               "#afffd7" aria-colors)
(puthash "Venezia"                                   "#afffd7" aria-colors)
(puthash "Deep-Venice"                               "#005f5f" aria-colors)
(puthash "Cyan-Blue"                                 "#5fafaf" aria-colors)

(puthash "Soft-lavender"                             "#afafd7" aria-colors)


(defun aria-color (name) (gethash name aria-colors))
