# ARIA theme for Emacs 24
ARIAをイメージして作ったEmacsのcolor-themeです。
黒背景です。

![Sample](https://bitbucket.org/cloverrose/dot.emacs.d/raw/46362fde1c5f88ac44c160a07a62107bd8214527/themes/screenshot/screenshot_main.png)

## 特徴
### 灯里・藍華・アリスが並ぶ姿をイメージ
 - if, return, privateなどキーワードは灯里ちゃんをイメージしたピンク
 - 型名は藍華ちゃんをイメージした青
 - 関数名はアリスちゃんをイメージした緑

Javaでよく出てくるメソッド定義が幸せです

![Java Sample](https://bitbucket.org/cloverrose/dot.emacs.d/raw/46362fde1c5f88ac44c160a07a62107bd8214527/themes/screenshot/screenshot_java.png)

### ミニバッファは灯里ちゃんとの対話をイメージ
プロンプトと補完・マッチ部分は灯里ちゃんをイメージしたピンク

### 検索はネオ・ヴェネツィアの運河を進むウンディーネをイメージ
 - マッチ部分はヴェネツィアの運河をイメージした淡い青緑色
 - ハイライトはウンディーネの制服のアクセントの明るい黄色

![Search Sample](https://bitbucket.org/cloverrose/dot.emacs.d/raw/46362fde1c5f88ac44c160a07a62107bd8214527/themes/screenshot/screenshot_search.png)

### その他の配色の特徴
 - modelineはヴェネツィアの運河をイメージした深い青緑色
 - 文字列は姫屋の壁の色をイメージしたオレンジ
 - コメントは見難くならない程度に背景に溶け込むイタリアの土色（目立つコメントに変更可）
 - 変数名は控えめな黄色（目立つ黄色に変更可）
 - ドキュメント文字列は黒に映えるオレンジ色でドキュメントの見栄えがよくなって、テンションが上がる
 - 組み込み、定数、プリプロセッサーはゴンドラや壁の塗装で使われる鮮やかな青
 - プログラミングするときに気をつけたい全角空白と末尾空白は鮮やかな青緑色で強調
 - moccur（moccur-edit）はカーソル上の文字が見やすいように配色を工夫


## Files
 - aria-theme.el Emacs24のthemeフレイムワークにしたがって作ったテーマ
 - aria-palette.el aria-theme.elで使う色を定義。使っていない色もたくさん入ってる。

aria-palette.elの色はaria-theme.el以外でも使えます。
自分は80文字目に縦線を入れるfciの色に使ってます。
```lisp
((setq fci-rule-color (aria-color "Renaissance")))
```

## How to use
~/.emacs.d/themesディレクトリにaria-theme.elとaria-palette.elを配置

init.elに以下を追加
```lisp
(defvar themespath (expand-file-name "~/.emacs.d/themes"))
(load (concat themespath "/aria-palette.el"))
(add-to-list 'custom-theme-load-path themespath)
(load-theme 'aria t)
```

（オプション 半透明の設定をする)
```lisp
(set-frame-parameter nil 'alpha 85)
```

## その他
次の項目も設定しています。（インストールしていなくてもエラーはでません）

 - linum
 - mcomplete
 - tabbar
 - whitespace
 - moccur(moccur-edit)
