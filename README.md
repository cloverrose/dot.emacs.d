# dot.emacs.d

## how to use
```shell
git clone git@bitbucket.org:cloverrose/dot.emacs.d.git
cd dot.emacs.d
./setup.sh
ln -s ~/dot.emacs.d/ ~/.emacs.d
```

## suport Emacs versions
- 24.3
